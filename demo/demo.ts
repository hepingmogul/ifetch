import IFetch, { FetchOptions, FetchResponse } from '../lib/index'
const ifetch = new IFetch('http://localhost:3003')
ifetch.interceptors.request((config: FetchOptions) => {
    const token = 'token'
    if (token) {
        config.headers!.set('Authorization', 'Bearer ' + token)
    }
    return config
})
ifetch.interceptors.response((response: FetchResponse) => {
    console.log('response', response)
    const { data } = response
    /**
    * 后端返回的对象
    */
    // 二进制对象
    if (data instanceof Blob) {
        return data
    }
    const { status, code } = data
    // 成功状态
    if (+code === 200) {
        return data
    }
    // 失败状态
    // 特殊状态外须业务层做处理
    switch (status) {
        case 401:
            data.msg = '登录已经过期，请重新登录！'
            break
        case 403:
            data.msg = '没有权限访问此站！'
            break
        case 404:
            data.msg = '找不到地址！'
            break
        default:
            data.msg = data.msg || '未知错误！'
            break
    }
    return Promise.reject(data)
})
ifetch.request({
    url: '/1',
    // timeout: 30
}).then((data) => {
    console.log('data', data)
}).catch((error) => {
    console.log('error', error)
})