// rollup.config.js
import typescript from '@rollup/plugin-typescript'
import babel from 'rollup-plugin-babel'

export default {
    input: 'demo.ts',
    output: {
        dir: 'dist',
        format: 'iife'
    },
    plugins: [
        typescript(),
        babel({
            exclude: 'node_modules/**',
            extensions: ['.js']
        }),
    ]
}