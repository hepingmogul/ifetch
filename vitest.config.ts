import { defineConfig } from 'vitest/config'
import eslintPlugin from 'vite-plugin-eslint'

export default defineConfig({
    test: {

    },
    plugins: [
        eslintPlugin({
            include: ['./**/*.ts']
        })
    ]
})
