type ResponseType = 'arrayBuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream';
type Method = 'GET' | 'DELETE' | 'HEAD' | 'OPTIONS' | 'POST' | 'PUT' | 'PATCH' | 'PURGE' | 'LINK' | 'UNLINK';
interface FetchOptions<P = any, D = any> extends RequestInit {
    url?: string;
    method?: Method;
    params?: P;
    data?: D;
    responseType?: ResponseType;
    timeout?: number;
    headers?: Headers;
}
interface FetchResponseOptions {
    url: string;
}
interface FetchResponse {
    status: number;
    statusText: string;
    data: any;
    options: FetchResponseOptions;
}
declare class Interceptors {
    private requestHandlers;
    requestEach(config: FetchOptions): Promise<any>;
    request(...args: any[]): void;
    private responseHandlers;
    responseEach(response: FetchResponse): Promise<any>;
    response(...args: any[]): void;
}
type CredentialsType = 'include' | 'omit' | 'same-origin' | undefined;
declare class IFetch {
    private baseURL;
    private timeout;
    private credentials;
    interceptors: Interceptors;
    constructor(baseURL?: string, timeout?: number | null, credentials?: CredentialsType);
    request<P, D>(options: FetchOptions<P, D>): Promise<any>;
    get(url: string, options?: FetchOptions): Promise<any>;
    post(url: string, options?: FetchOptions): Promise<any>;
    put(url: string, options?: FetchOptions): Promise<any>;
    delete(url: string, options?: FetchOptions): Promise<any>;
}

export { type CredentialsType, type FetchOptions, type FetchResponse, type Method, type ResponseType, IFetch as default };
