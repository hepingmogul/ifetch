// https://www.rollupjs.com/configuration-options/#output-format
import typescript from '@rollup/plugin-typescript'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import babel from 'rollup-plugin-babel'
import { dts } from 'rollup-plugin-dts'
import eslint from '@rollup/plugin-eslint'

export default [
    {
        input: 'src/index.ts',
        output: {
            format: 'es',
            file: 'lib/index.js'
        },
        plugins: [
            eslint({
                include: ['src/**/*.ts'],
                exclude: ['node_modules/**'],
                // formatter: 'compact',
                // overrideConfig: eslintConfig
                // overrideConfigFile: './.eslintrc'
            }),
            nodeResolve({
                extensions: ['.js']
            }),
            typescript(),
            babel({
                exclude: 'node_modules/**',
                extensions: ['.js']
            }),
        ]
    },
    /* 单独生成声明文件 */
    {
        input: 'src/index.ts',
        output: {
            format: 'es',
            file: 'lib/index.d.ts',
        },
        plugins: [dts()],
    },
]