import IFetch, { /* FetchOptions, */ FetchResponse } from '../lib/index'

import { listen, Listener } from 'listhen'
import { getQuery, joinURL } from 'ufo'
import {
    createApp,
    createError,
    eventHandler,
    readBody,
    // readRawBody,
    // readMultipartFormData,
    toNodeListener,
} from 'h3'

// https://cn.vitest.dev/
// index.test.js
import { expect, it, describe, beforeAll, afterAll } from 'vitest'

const formDataParse = (body, boundary) => {
    //将Buffer类型的数据转化成binary编码格式的字符串
    const formStr = Buffer.concat(body).toString('binary')
    const formarr = formStr.split(boundary)
    //去掉首尾两端的无用字符
    formarr.shift()
    formarr.pop()
    //存储普通key-value
    const filed = {}
    //存储文件信息
    const file = {}
    for (let item of formarr) {
        //去除首尾两端的非信息字符
        item = item.slice(0, -2).trim()
        //value存储input输入的值
        let value = ''
        //不同操作系统换行符不同,用变量a声明特殊分割点位的下标
        let a
        if ((a = item.indexOf('\r\n\r\n')) !== -1) {
            value = item.slice(a + 4)
        } else if ((a = item.indexOf('\r\r')) !== -1) {
            value = item.slice(a + 2)
        } else if ((a = item.indexOf('\n\n')) !== -1) {
            value = item.slice(a + 2)
        }
        //正则匹配，组中内容
        const key = item.match(/name="([^"]+)"/)?.[1] || ''
        if (item.indexOf('filename') === -1) {
            if (!(key in filed)) {
                //将二进制字符串转化成utf8格式的字符串
                filed[key] = Buffer.from(value, 'binary').toString('utf8')
            } else {
                //将复选框的数据放入一个数组中
                const arr = []
                filed[key] = arr.concat(filed[key], value)
            }
        } else {
            const filename_b = item.match(/filename="([^"]*)"/)?.[1] || ''
            //解决中文文件名乱码的问题
            const filename = Buffer.from(filename_b, 'binary').toString()
            const contentType = item.slice(item.indexOf('Content-Type:'), a)
            const obj = {
                filename: '',
                contentType: '',
                binaryStream: ''
            }
            obj.filename = filename
            obj.contentType = contentType
            obj.binaryStream = value//文件的二进制数据
            const arr = []
            if (!(key in file)) {
                arr.push(obj)
                file[key] = arr
            } else {
                //用于多文件上传
                file[key] = arr.concat(file[key], obj)
            }
        }
    }
    return { filed, file }
}

describe('ifetch', () => {
    let listener: Listener
    let ifetch: IFetch
    const getURL = (url: string) => joinURL(listener.url, url)

    beforeAll(async () => {
        const app = createApp()
            .use(
                '/ok',
                eventHandler(() => 'ok')
            )
            .use(
                '/get-header-uuid',
                eventHandler((event) => event.node.req.headers['uuid'])
            )
            .use(
                '/params',
                eventHandler((event) => getQuery(event.node.req.url || ''))
            )
            .use(
                '/binary',
                eventHandler((event) => {
                    event.node.res.setHeader('Content-Type', 'application/octet-stream')
                    return new Blob(['binary'])
                })
            )
            .use(
                '/404',
                eventHandler(() => {
                    // throw { status: 404, statusText: 'Not Fund' }
                    return createError({ status: 404, statusText: 'Not Fund' })
                })
            )
            .use(
                '/post',
                eventHandler(async (event) => {
                    return {
                        data: await readBody(event),
                        code: 200
                    }
                })
            )
            .use(
                '/formdata',
                eventHandler(async (event) => {
                    const result = new Promise((resolve) => {
                        const req = event.node.req
                        // const res = event.node.res
                        const body: string[] = []
                        const contentType: string = req.headers['content-type'] || ''
                        const boundary = contentType.split('boundary=')[1]
                        //console.log(boundary);
                        req.on('data', (chunk: string) => {
                            body.push(chunk)
                        }).on('end', async () => {
                            const { filed } = formDataParse(body, boundary)
                            // console.log('filed', filed)
                            resolve(filed)
                        })
                    })
                    const data = await result
                    return {
                        data,
                        code: 200
                    }
                })
            )
        listener = await listen(toNodeListener(app))
        ifetch = new IFetch(listener.url)
        // ifetch.interceptors.request((config: FetchOptions) => {
        //     return config
        // })
        ifetch.interceptors.response((response: FetchResponse) => {
            const { data } = response
            return data
        })
    })

    afterAll(() => {
        listener.close().catch(console.error)
    })

    it('ok', async () => {
        expect(
            await ifetch.request({ url: 'ok' })
        ).to.equal('ok')

        expect(
            await ifetch.get(getURL('ok'))
        ).to.equal('ok')
    })

    it('specifying responseType', async () => {
        expect(
            await ifetch.request({
                url: 'params?test=true',
                // responseType: 'json'
            })
        ).to.deep.equal({ test: 'true' })

        expect(
            await ifetch.request({
                url: 'params?test=true',
                responseType: 'blob'
            })
        ).to.be.instanceOf(Blob)

        expect(
            await ifetch.request({
                url: getURL('params?test=true'),
                responseType: 'arrayBuffer'
            })
        ).to.be.instanceOf(ArrayBuffer)
    })

    it('returns a blob for binary content-type', async () => {
        expect(
            await ifetch.request({
                url: getURL('binary')
            })
        ).to.be.instanceOf(Blob)
    })

    it('exception', async () => {
        expect(
            await ifetch.get('404').catch((err) => {
                console.log('exception:', err)
                return err.statusCode
            })
        ).to.equal(404)
    })

    it('post data', async () => {
        const postData = { foo: 'bar' }
        const { data } = await ifetch.post('post', { data: postData })
        console.log('post data:', data)
        expect(data).toStrictEqual(
            JSON.stringify(postData)
        )
    })

    it('post FormData', async () => {
        const postData = { foo: 'bar' }
        const fd = new FormData()
        Object.keys(postData).forEach(key => fd.append(key, postData[key]))
        const { data } = await ifetch.post('formdata', { data: fd })
        console.log('post FormData:', data)
        expect(data).toStrictEqual(postData)
    })

    it('HEAD no content', async () => {
        const res = await ifetch.request({ url: 'ok', method: 'HEAD' })
        console.log('HEAD no content:', res)
        expect(res).toBe('')
    })

    it('aborting on timeout', async () => {
        try {
            await ifetch.request({ url: 'ok', timeout: 1 })
        } catch (error) {
            console.log('aborting on timeout:', error.message)
            expect(error.message).toBe('The operation was aborted.')
        }
    })
    it('get header uuid', async () => {
        const id = Math.random().toString(16)
        const res = await ifetch.request({ url: 'get-header-uuid', headers: { uuid: id } })
        expect(res).toBe(id)
    })
})
