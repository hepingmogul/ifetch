// `export default A` 与 `export { A as default }` 是不同的
// https://juejin.cn/post/7021207181861077028

// module.js
import { hello } from './main.js';

hello();

export const foo = () => console.log('foo');
