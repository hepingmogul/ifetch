import IFetch from '../lib/index.js'

// class A {
//     prop = 'A'
//     constructor() {
//         console.log('A.constructor')
//     }

//     create() {
//         console.log('A.create', this.prop)
//     }
// }


// const _a = () => {
//     const ins = new A()
//     return ins.create.bind(ins)
// }


// console.log('__', _a()())

const fd = new FormData()
fd.append('a', 1)
fd.append('b', 2)
// fetch('https://api.petslib.cn/frontend/friend/link', {
new IFetch().post('http://localhost:3003/frontend/friend/link', {
    method: 'post',
    body: fd
}).then((res) => {
    console.log(res)
})

