# ifetch

Fetch API 提供了一个 JavaScript 接口，用于访问和操纵 HTTP 管道的一些具体部分，例如请求和响应。它还提供了一个全局 fetch() 方法，该方法提供了一种简单，合理的方式来跨网络异步获取资源。  


- demo
    - demo.ts
        ```ts
        import IFetch, { FetchOptions, FetchResponse } from '../lib/index'
        const ifetch = new IFetch('http://localhost:3003')
        ifetch.interceptors.request((config: FetchOptions) => {
            const token = 'token'
            if (token) {
                config.headers!.set('Authorization', 'Bearer ' + token)
            }
            return config
        })
        ifetch.interceptors.response((response: FetchResponse) => {
            console.log('response', response)
            const { data } = response
            /**
            * 后端返回的对象
            */
            // 二进制对象
            if (data instanceof Blob) {
                return data
            }
            const { status, code } = data
            // 成功状态
            if (+code === 200) {
                return data
            }
            // 失败状态
            // 特殊状态外须业务层做处理
            switch (status) {
                case 401:
                    data.msg = '登录已经过期，请重新登录！'
                    break
                case 403:
                    data.msg = '没有权限访问此站！'
                    break
                case 404:
                    data.msg = '找不到地址！'
                    break
                default:
                    data.msg = data.msg || '未知错误！'
                    break
            }
            return Promise.reject(data)
        })
        ifetch.request({
            url: '/1',
            // timeout: 30
        }).then((data) => {
            console.log('data', data)
        }).catch((error) => {
            console.log('error', error)
        })
        ```
    - demo.html
        ```html
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>

        <body>
            <script src="./demo.js"></script>
        </body>

        </html>
        ```